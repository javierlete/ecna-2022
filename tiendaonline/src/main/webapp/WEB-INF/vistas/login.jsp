<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/vistas/includes/cabecera.jsp"%>

<form action="login" method="post">
	<div class="mb-3 row">
		<label class="col-sm-2 form-label" for="email">Email</label>
		<div class="col-sm-10">
			<input class="form-control" type="email" name="email" placeholder="Email"
				value="${usuario.email}">
		</div>
	</div>
	<div class="mb-3 row">
		<label class="col-sm-2 form-label" for="password">password</label>
		<div class="col-sm-10">
			<input class="form-control" type="password" name="password" placeholder="password">
		</div>
	</div>
		
	<div class="mb-3 row">
		<div class="offset-sm-2 scol-sm-10">
			<button class="btn btn-primary">Login</button>
			<span class="text-danger">${error}</span>
		</div>
	</div>
	
	
</form>

<%@ include file="/WEB-INF/vistas/includes/pie.jsp"%>
