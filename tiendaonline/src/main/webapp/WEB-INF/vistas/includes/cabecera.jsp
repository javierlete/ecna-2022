<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%--
Las taglib que estamos referenciando abajo son las JSTL
(Jakarta Standard Tag Library)

Tienen 5 bibliotecas de etiquetas.

Las más importantes son c (core) y fmt (formato)

c: forEach, if, choose
fmt: formatNumber

Necesitamos la dependencia.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html class="min-vh-100">
<head>
<meta charset="UTF-8">
<title>Productos</title>

<base href="${pageContext.request.contextPath}/">

<link rel="icon" type="image/x-icon" href="favicon.ico" />

<link rel="stylesheet" href="css/bootstrap-icons.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.bundle.min.js"></script>

</head>
<body class="container-fluid d-flex flex-column min-vh-100">
	<nav
		class="navbar navbar-expand-lg bg-dark navbar-dark mb-3 sticky-top">
		<div class="container-fluid">
			<a class="navbar-brand" href="#">TiendaOnline</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item"><a class="nav-link" href="#">Principal</a></li>
				</ul>
				<ul class="navbar-nav mb-2 mb-lg-0">
					<c:if test="${sessionScope.usuario != null}">
						<li class="nav-item"><a class="nav-link"
							href="admin/productos">Administración</a></li>
						<li class="nav-item"><a class="nav-link" href="logout">Logout</a></li>
						<li class="navbar-text">${sessionScope.usuario.email}</li>
					</c:if>
					<c:if test="${sessionScope.usuario == null}">
						<li class="nav-item"><a class="nav-link" href="login">Login</a></li>
					</c:if>
				</ul>

			</div>
		</div>
	</nav>