<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<footer class="bg-dark text-light p-3 mt-auto d-flex">
	<p class="me-auto mb-0">&copy;2022 Javier Lete</p>
	<p class="mb-0">
		<a class="text-light" href="#"><i class="bi bi-facebook"></i></a>
		<a class="text-light" href="#"><i class="bi bi-twitter"></i></a>
		<a class="text-light" href="#"><i class="bi bi-youtube"></i></a>
		<a class="text-light" href="#"><i class="bi bi-instagram"></i></a>
	</p>
</footer>

</body>
</html>
