package es.ecna.curso.tiendaonline.controladores.admin;

import java.io.IOException;

import es.ecna.curso.tiendaonline.controladores.Globales;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/admin/productos")
public class ProductosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("productos", Globales.DAO.obtenerTodos());
		request.getRequestDispatcher("/WEB-INF/vistas/admin/productos.jsp").forward(request, response);
	}

}
