package es.ecna.curso.tiendaonline.accesodatos;

// El interface Dao es abstracto y está preparado para recibir
// un tipo T cualquiera

// Data Access Object
public interface Dao<T> {
	Iterable<T> obtenerTodos();
	T obtenerPorId(Long id);
	
	T insertar(T objeto);
	T modificar(T objeto);
	void borrar(Long id);
}
