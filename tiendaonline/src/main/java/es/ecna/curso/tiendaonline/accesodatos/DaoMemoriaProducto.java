package es.ecna.curso.tiendaonline.accesodatos;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.TreeMap;

import es.ecna.curso.tiendaonline.modelos.Producto;

public class DaoMemoriaProducto implements Dao<Producto> {
	private static TreeMap<Long, Producto> productos = new TreeMap<>();
	
	static {
		productos.put(1L, new Producto(1L, "Producto1", new BigDecimal("123.1"), LocalDate.of(2001, 11, 12)));
		productos.put(2L, new Producto(2L, "Producto2", new BigDecimal("223.1"), LocalDate.of(2002, 11, 12)));
		productos.put(3L, new Producto(3L, "Producto3", new BigDecimal("323.1"), LocalDate.of(2003, 11, 12)));
		productos.put(4L, new Producto(4L, "Producto4", new BigDecimal("423.1"), LocalDate.of(2004, 11, 12)));		
	}
	
	// SINGLETON
	private DaoMemoriaProducto() {}
	private static final DaoMemoriaProducto INSTANCIA = new DaoMemoriaProducto();
	public static DaoMemoriaProducto getInstancia() { return INSTANCIA; } 
	// FIN SINGLETON
	
	@Override
	public Iterable<Producto> obtenerTodos() {
		return productos.values();
	}

	@Override
	public Producto obtenerPorId(Long id) {
		return productos.get(id);
	}

	@Override
	public Producto insertar(Producto producto) {
		Long id = productos.size() > 0 ? productos.lastKey() + 1L : 1L;
		producto.setId(id);
		productos.put(id, producto);
		
		return producto;
	}

	@Override
	public Producto modificar(Producto producto) {
		if(!productos.containsKey(producto.getId())) {
			throw new AccesoDatosException("No se puede modificar un registro inexistente");
		}
		
		productos.put(producto.getId(), producto);
		
		return producto;
	}

	@Override
	public void borrar(Long id) {
		if(!productos.containsKey(id)) {
			throw new AccesoDatosException("No se puede borrar un registro inexistente");
		}
		productos.remove(id);
	}

}
