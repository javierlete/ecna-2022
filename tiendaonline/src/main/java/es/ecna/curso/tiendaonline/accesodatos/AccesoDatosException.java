package es.ecna.curso.tiendaonline.accesodatos;

import lombok.experimental.StandardException;

@StandardException
public class AccesoDatosException extends RuntimeException {

	private static final long serialVersionUID = 1L;

}
