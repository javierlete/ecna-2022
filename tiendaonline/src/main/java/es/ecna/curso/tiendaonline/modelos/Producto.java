package es.ecna.curso.tiendaonline.modelos;

import java.math.BigDecimal;
import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "productos")
public class Producto {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nombre;
	private BigDecimal precio;
	private LocalDate fechaCaducidad;
	
	public Producto(String id, String nombre, String precio, String fechaCaducidad) {
		setId(id);
		setNombre(nombre);
		setPrecio(precio);
		setFechaCaducidad(fechaCaducidad);
	}
	
	public void setId(String id) {
		if(id == null) return;
		
		if(id.trim().length() != 0) {
			setId(Long.parseLong(id));
		}
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setPrecio(String precio) {
		if(precio == null) return;
		
		if(precio.trim().length() != 0) {
			setPrecio(new BigDecimal(precio));
		}
	}
	
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}
	
	public void setFechaCaducidad(String fechaCaducidad) {
		if(fechaCaducidad == null) return;
		
		if(fechaCaducidad.trim().length() != 0) {
			setFechaCaducidad(LocalDate.parse(fechaCaducidad));
		}
	}
	
	public void setFechaCaducidad(LocalDate fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}
}
