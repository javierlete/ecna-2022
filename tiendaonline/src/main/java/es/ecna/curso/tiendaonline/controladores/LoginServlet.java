package es.ecna.curso.tiendaonline.controladores;

import java.io.IOException;

import es.ecna.curso.tiendaonline.modelos.Usuario;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/vistas/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Recoger la información de la petición
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		// Creación de modelo en base a la información
		Usuario usuario = new Usuario(null, email, password);
		
		// Ejecución de la lógica de negocio
		if(validar(usuario)) {
			// Empaquetar nuevos modelos...
			HttpSession session = request.getSession();
			session.setAttribute("usuario", usuario); // ATRIBUTO DE SESIÓN
			
			// ...para la siguiente pantalla
			response.sendRedirect(request.getContextPath() + "/admin/productos"); // REDIRECCIÓN: el navegador vuelve a hacer otra petición (se pierden los datos de request), pero actualiza la URL en el navegador
		} else {
			// Empaquetar nuevos modelos...
			request.setAttribute("usuario", usuario); // ATRIBUTO DE REQUEST
			request.setAttribute("error", "El usuario o contraseña es incorrecto");
			
			// ...para la siguiente pantalla
			request.getRequestDispatcher("/WEB-INF/vistas/login.jsp").forward(request, response); // FORWARD: el servidor reenvía la petición a la JSP incluyendo todos los datos anterior (request), pero no modifica la URL
		}
	}

	private boolean validar(Usuario usuario) {
		return "admin@email.net".equals(usuario.getEmail()) && "contra".equals(usuario.getPassword());
	}

}
