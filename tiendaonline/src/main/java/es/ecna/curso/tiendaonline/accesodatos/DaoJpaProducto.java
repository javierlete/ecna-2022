package es.ecna.curso.tiendaonline.accesodatos;

import java.util.List;

import es.ecna.curso.tiendaonline.modelos.Producto;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class DaoJpaProducto implements Dao<Producto> {

	private static final EntityManagerFactory EMF = Persistence
			.createEntityManagerFactory("es.ecna.curso.tiendaonline.modelos");

	private EntityManager crearTransaccion() {
		EntityManager em = EMF.createEntityManager();
		em.getTransaction().begin();
		
		return em;
	}
	
	private void cerrarTransaccion(EntityManager em) {
		em.getTransaction().commit();
		em.close();
	}
	
	@Override
	public Iterable<Producto> obtenerTodos() {
		EntityManager em = crearTransaccion();
		
		List<Producto> productos= em.createQuery("from Producto", Producto.class).getResultList();
		
		cerrarTransaccion(em);
		
		return productos;
	}

	@Override
	public Producto obtenerPorId(Long id) {
		EntityManager em = crearTransaccion();
		
		Producto producto = em.find(Producto.class, id);
		
		cerrarTransaccion(em);
		
		return producto;
	}

	@Override
	public Producto insertar(Producto producto) {
		EntityManager em = crearTransaccion();
		
		em.persist(producto);
		
		cerrarTransaccion(em);
		
		return producto;
	}

	@Override
	public Producto modificar(Producto producto) {
		EntityManager em = crearTransaccion();
		
		em.merge(producto);
		
		cerrarTransaccion(em);
		
		return producto;
	}

	@Override
	public void borrar(Long id) {
		EntityManager em = crearTransaccion();
		
		em.remove(em.find(Producto.class, id));
		
		cerrarTransaccion(em);
	}

}
