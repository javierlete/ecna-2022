package es.ecna.curso.tiendaonline.accesodatos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.Date;

import es.ecna.curso.tiendaonline.modelos.Producto;

public class DaoSQLiteProducto implements Dao<Producto> {
	private final String url;
	// private static final String URL = "jdbc:mysql://localhost:3306/tiendaonline";
	// private static final String USER = "root";
	// private static final String PASS = "admin";

	private static final String SQL_SELECT = "SELECT * FROM productos";
	private static final String SQL_SELECT_ID = "SELECT * FROM productos WHERE id = ?";
	private static final String SQL_INSERT = "INSERT INTO productos (nombre, precio, fecha_caducidad) VALUES (?,?,?)";
	private static final String SQL_UPDATE = "UPDATE productos SET nombre=?, precio=?, fecha_caducidad=? WHERE id = ?";
	private static final String SQL_DELETE = "DELETE FROM productos WHERE id = ?";

	static {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			throw new AccesoDatosException("No se ha encontrado el driver de SQLite", e);
		}
	}

	public DaoSQLiteProducto(String fichero) {
		url = "jdbc:sqlite:" + fichero;
	}

	private Connection getConexion() {
		try {
			return DriverManager.getConnection(url);// , USER, PASS);
		} catch (SQLException e) {
			throw new AccesoDatosException("No se ha podido conectar con la base de datos", e);
		}
	}

	@Override
	public Iterable<Producto> obtenerTodos() {
		try (Connection con = getConexion();
				PreparedStatement pst = con.prepareStatement(SQL_SELECT);
				ResultSet rs = pst.executeQuery()) {
			ArrayList<Producto> productos = new ArrayList<>();

			LocalDate fechaCaducidad;
			Date fecha;

			while (rs.next()) {
				fecha = rs.getDate("fecha_caducidad");
				
				if (fecha == null) {
					fechaCaducidad = null;
				} else {
					fechaCaducidad = fecha.toLocalDate();
				}
				
				productos.add(new Producto(rs.getLong("id"), rs.getString("nombre"), rs.getBigDecimal("precio"),
						fechaCaducidad));
			}

			return productos;
		} catch (SQLException e) {
			throw new AccesoDatosException("No se han podido obtener los registros", e);
		}
	}

	@Override
	public Producto obtenerPorId(Long id) {
		try (Connection con = getConexion(); PreparedStatement pst = con.prepareStatement(SQL_SELECT_ID);) {

			pst.setLong(1, id);

			ResultSet rs = pst.executeQuery();
			Producto producto = null;
			LocalDate fechaCaducidad;
			Date fecha;

			if (rs.next()) {
				fecha = rs.getDate("fecha_caducidad");
				
				if (fecha == null) {
					fechaCaducidad = null;
				} else {
					fechaCaducidad = fecha.toLocalDate();
				}
				
				producto = new Producto(rs.getLong("id"), rs.getString("nombre"), rs.getBigDecimal("precio"),
						fechaCaducidad);
			}

			return producto;
		} catch (SQLException e) {
			throw new AccesoDatosException("No se ha podido obtener el registro", e);
		}
	}

	@Override
	public Producto insertar(Producto producto) {
		try (Connection con = getConexion(); PreparedStatement pst = con.prepareStatement(SQL_INSERT);) {

			pst.setString(1, producto.getNombre());
			pst.setBigDecimal(2, producto.getPrecio());
			pst.setDate(3, java.sql.Date.valueOf(producto.getFechaCaducidad()));

			int numRegMod = pst.executeUpdate();

			if (numRegMod != 1) {
				throw new AccesoDatosException("Se ha insertado un número de registros " + numRegMod);
			}

			return producto;
		} catch (SQLException e) {
			throw new AccesoDatosException("No se ha podido insertar el registro", e);
		}
	}

	@Override
	public Producto modificar(Producto producto) {
		try (Connection con = getConexion(); PreparedStatement pst = con.prepareStatement(SQL_UPDATE);) {

			pst.setString(1, producto.getNombre());
			pst.setBigDecimal(2, producto.getPrecio());
			pst.setDate(3, java.sql.Date.valueOf(producto.getFechaCaducidad()));
			pst.setLong(4, producto.getId());

			int numRegMod = pst.executeUpdate();

			if (numRegMod != 1) {
				throw new AccesoDatosException("Se ha modificado un número de registros " + numRegMod);
			}

			return producto;
		} catch (SQLException e) {
			throw new AccesoDatosException("No se ha podido modificar el registro", e);
		}
	}

	@Override
	public void borrar(Long id) {
		try (Connection con = getConexion(); PreparedStatement pst = con.prepareStatement(SQL_DELETE);) {

			pst.setLong(1, id);

			int numRegMod = pst.executeUpdate();

			if (numRegMod != 1) {
				throw new AccesoDatosException("Se ha borrado un número de registros " + numRegMod);
			}

		} catch (SQLException e) {
			throw new AccesoDatosException("No se ha podido borrar el registro", e);
		}
	}

}
