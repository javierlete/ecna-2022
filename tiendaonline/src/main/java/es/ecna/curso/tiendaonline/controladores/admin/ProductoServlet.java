package es.ecna.curso.tiendaonline.controladores.admin;

import java.io.IOException;

import es.ecna.curso.tiendaonline.controladores.Globales;
import es.ecna.curso.tiendaonline.modelos.Producto;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/admin/producto")
public class ProductoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");

		if (id != null) {
			Producto producto = Globales.DAO.obtenerPorId(Long.parseLong(id));
			request.setAttribute("producto", producto);
		}

		request.getRequestDispatcher("/WEB-INF/vistas/admin/producto.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		// Recoger la información de la petición
		String id = request.getParameter("id");
		String nombre = request.getParameter("nombre");
		String precio = request.getParameter("precio");
		String fechaCaducidad = request.getParameter("fechaCaducidad");

		// Creación de modelo en base a la información
		Producto producto = new Producto(id, nombre, precio, fechaCaducidad);

		// Ejecución de la lógica de negocio
		if (producto.getId() == null) {
			Globales.DAO.insertar(producto);
		} else {
			Globales.DAO.modificar(producto);
		}

		// ...para la siguiente pantalla
		response.sendRedirect("productos");

	}

}
