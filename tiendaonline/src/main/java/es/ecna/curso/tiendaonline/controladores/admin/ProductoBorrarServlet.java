package es.ecna.curso.tiendaonline.controladores.admin;

import java.io.IOException;

import es.ecna.curso.tiendaonline.controladores.Globales;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/admin/producto/borrar")
public class ProductoBorrarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");
		
		Globales.DAO.borrar(Long.parseLong(id));
		
		response.sendRedirect("../productos");
	}

}
