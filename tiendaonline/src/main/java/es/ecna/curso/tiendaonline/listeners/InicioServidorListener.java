package es.ecna.curso.tiendaonline.listeners;

import es.ecna.curso.tiendaonline.accesodatos.DaoJpaProducto;
import es.ecna.curso.tiendaonline.controladores.Globales;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

@WebListener
public class InicioServidorListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent sce)  { 
    	// String realPath = sce.getServletContext().getRealPath("/WEB-INF");
    	Globales.DAO = new DaoJpaProducto(); //new DaoSQLiteProducto(realPath + "/sql/tiendaonline.db");
    }
	
}
