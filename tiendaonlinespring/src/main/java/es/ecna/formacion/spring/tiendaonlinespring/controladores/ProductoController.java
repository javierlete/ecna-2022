package es.ecna.formacion.spring.tiendaonlinespring.controladores;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import es.ecna.formacion.spring.tiendaonlinespring.dtos.ProductoDTO;
import es.ecna.formacion.spring.tiendaonlinespring.entidades.Producto;
import es.ecna.formacion.spring.tiendaonlinespring.servicios.ProductoService;

@Controller
public class ProductoController {
	private static final String REDIRECT_PRODUCTOS = "redirect:/productos";
	private static final String VISTA_PRODUCTO = "producto";

	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private ProductoService servicio;
	
	@GetMapping({"/", "/productos"})
	public String productos(Model modelo) {
		modelo.addAttribute("productos", servicio.obtenerTodos());
		modelo.addAttribute("mensaje", "Se han cargado los productos");
		return "productos";
	}
	
	@GetMapping({"/producto", "/producto/{id}"})
	public String productoGet(@PathVariable(required = false) Long id, Model modelo) {
		Producto producto;
		
		if(id != null) {
			producto = servicio.obtenerPorId(id).orElse(null);
		} else {
			producto = new Producto();
		}
		
		if(producto != null) { 
			modelo.addAttribute(producto);
		}
		
		return VISTA_PRODUCTO;
	}
	
	@PostMapping("/producto")
	public String productoPostNuevo(@Valid ProductoDTO productoDTO, BindingResult bindingResult) {
		if(servicio.guardar(productoDTO, bindingResult)) {
			return REDIRECT_PRODUCTOS;
		} else {
			return VISTA_PRODUCTO;
		}
	}
	
	@PostMapping("/productoViejo")
	public String productoPost(@Valid ProductoDTO productoDTO, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			return VISTA_PRODUCTO;
		}
		
		Producto producto = mapper.map(productoDTO, Producto.class);
		
		if(producto.getId() != null) {
			servicio.modificar(producto);
		} else {
			servicio.agregar(producto);
		}
		
		return REDIRECT_PRODUCTOS;
	}
	
	@GetMapping("/producto/borrar/{id}")
	public String productoBorrar(@PathVariable Long id) {
		servicio.borrar(id);
		
		return REDIRECT_PRODUCTOS;
	}
}
