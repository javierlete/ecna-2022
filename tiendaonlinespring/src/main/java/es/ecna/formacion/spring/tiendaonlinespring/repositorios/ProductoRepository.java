package es.ecna.formacion.spring.tiendaonlinespring.repositorios;

import java.util.Set;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import es.ecna.formacion.spring.tiendaonlinespring.entidades.Producto;

@RepositoryRestResource(collectionResourceRel = "productos", path = "productos")
public interface ProductoRepository extends PagingAndSortingRepository<Producto, Long> {
	Set<Producto> findByNombreLike(String nombre);
}
