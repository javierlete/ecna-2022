package es.ecna.formacion.spring.tiendaonlinespring.servicios;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.validation.BindingResult;

import es.ecna.formacion.spring.tiendaonlinespring.dtos.ProductoDTO;
import es.ecna.formacion.spring.tiendaonlinespring.entidades.Producto;

public interface ProductoService {

	Iterable<Producto> obtenerTodos();
	Optional<Producto> obtenerPorId(Long id);
	Producto agregar(Producto producto);
	Producto modificar(Producto producto);
	void borrar(Long id);
	boolean guardar(@Valid ProductoDTO productoDTO, BindingResult bindingResult);
}