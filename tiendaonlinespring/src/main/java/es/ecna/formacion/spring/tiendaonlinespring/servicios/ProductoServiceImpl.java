package es.ecna.formacion.spring.tiendaonlinespring.servicios;

import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import es.ecna.formacion.spring.tiendaonlinespring.dtos.ProductoDTO;
import es.ecna.formacion.spring.tiendaonlinespring.entidades.Producto;
import es.ecna.formacion.spring.tiendaonlinespring.repositorios.ProductoRepository;
import lombok.extern.java.Log;

@Log
@Service
public class ProductoServiceImpl implements ProductoService {

	
	@Autowired
	private ProductoRepository repo;
	
	@Autowired
	private ModelMapper mapper;

	@Override
	public Iterable<Producto> obtenerTodos() {
		log.info("Se han pedido todos los productos");
		
		Iterable<Producto> productos = repo.findAll();
		
		log.info("Ya se han obtenido todos los productos");
		
		return productos;
	}
	
	@Override
	public Optional<Producto> obtenerPorId(Long id) {
		return repo.findById(id);
	}

	@Override
	public Producto agregar(Producto producto) {
		return repo.save(producto);
	}

	@Override
	public Producto modificar(Producto producto) {
		return repo.save(producto);
	}

	@Override
	public void borrar(Long id) {
		repo.deleteById(id);
	}

	@Override
	public boolean guardar(@Valid ProductoDTO productoDTO, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			return false;
		}
		
		Producto producto = mapper.map(productoDTO, Producto.class);
		
		if(producto.getId() != null) {
			modificar(producto);
		} else {
			agregar(producto);
		}
		
		return true;
	}
}
