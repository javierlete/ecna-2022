package es.ecna.formacion.spring.tiendaonlinespring.servicios;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.validation.BindingResult;

import es.ecna.formacion.spring.tiendaonlinespring.dtos.ProductoDTO;
import es.ecna.formacion.spring.tiendaonlinespring.entidades.Producto;

public class ProductoServicePrueba implements ProductoService {

	@Override
	public Iterable<Producto> obtenerTodos() {
		ArrayList<Producto> productos = new ArrayList<>();
		
		for(Integer i = 1; i <= 5; i++) {
			productos.add(new Producto(i.longValue(), "Producto" + i, new BigDecimal(i*10), LocalDate.now(), i*2));
		}
		
		return productos;
	}

	@Override
	public Optional<Producto> obtenerPorId(Long id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Producto agregar(Producto producto) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Producto modificar(Producto producto) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void borrar(Long id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean guardar(ProductoDTO productoDTO, BindingResult bindingResult) {
		throw new UnsupportedOperationException();
	}

}
