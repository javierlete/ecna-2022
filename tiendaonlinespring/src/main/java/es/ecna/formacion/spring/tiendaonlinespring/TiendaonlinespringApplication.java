package es.ecna.formacion.spring.tiendaonlinespring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TiendaonlinespringApplication {

	public static void main(String[] args) {
		SpringApplication.run(TiendaonlinespringApplication.class, args);
	}

}
