package es.ecna.formacion.spring.tiendaonlinespring.entidades;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Future;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "productos")
public class Producto {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	@NotBlank
	@Size(min = 3, max = 50)
	private String nombre;

	@NotNull
	@DecimalMin("0")
	private BigDecimal precio;
	
	@DateTimeFormat(iso = ISO.DATE)
	@Future
	//@Column(name = "fecha_caducidad")
	private LocalDate fechaCaducidad;
	
	@NotNull
	@Min(0)
	private Integer stock;
}
