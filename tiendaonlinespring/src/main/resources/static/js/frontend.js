window.addEventListener('DOMContentLoaded', async function() {
	const respuesta = await fetch('http://localhost:8080/api/productos/');
	const datos = await respuesta.json();
	const productos = datos._embedded.productos;
	
	const tbody = document.querySelector('tbody');
	
	let tr;
	
	for(let producto of productos) {
		tr = document.createElement('tr');
		
		tr.innerHTML = `
			<th>${producto.id}</th>
			<td>${producto.nombre}</td>
			<td>${producto.precio}</td>
			<td>${producto.fechaCaducidad}</td>
			<td>${producto.stock}</td>
			<td>
				<a class="btn btn-primary btn-sm" href="javascript:editar(${producto.id})">Editar</a>
				<a class="btn btn-danger btn-sm" href="javascript:borrar(${producto.id})">Borrar</a>
			`;
		
		tbody.appendChild(tr);
	} 
});

function agregar() {
	alert('agregar');
}

function editar(id) {
	alert('editar ' + id);
}

function borrar(id) {
	alert('borrar ' + id);
}
