package es.ecna.formacion.spring.tiendaonlinespring;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import es.ecna.formacion.spring.tiendaonlinespring.entidades.Producto;
import es.ecna.formacion.spring.tiendaonlinespring.repositorios.ProductoRepository;

@SpringBootTest
class TiendaonlinespringApplicationTests {
	@Autowired
	private ProductoRepository repo;
	
	@Test
	void contextLoads() {
		repo.save(new Producto(null, "Prueba", new BigDecimal("100"), LocalDate.of(2023, 1, 1), 5));
		Set<Producto> productos = repo.findByNombreLike("rue");
		
		assertEquals(0, productos.size());
	}
	
	@Test
	@Transactional()
	void transaccion() {
		System.out.println("TRANSACCION UNO ANTES");
		repo.save(new Producto(null, "UNO", new BigDecimal("100"), LocalDate.of(2023, 1, 1), 5));
		System.out.println("TRANSACCION UNO DESPUES");
		//repo.deleteById(7L);
		System.out.println("TRANSACCION DOS ANTES");
		repo.save(new Producto(null, "DOS", new BigDecimal("100"), LocalDate.of(2023, 1, 1), 5));
		System.out.println("TRANSACCION DOS DESPUES");
	}

}
