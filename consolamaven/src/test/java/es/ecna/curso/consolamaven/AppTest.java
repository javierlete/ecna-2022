package es.ecna.curso.consolamaven;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AppTest 
{
    @Test
	public void sumar() {
		int x = App.sumar(5,2);
		
		assertEquals(7, x);
	}
}
